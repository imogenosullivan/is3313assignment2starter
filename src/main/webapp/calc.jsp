<%-- 
    Document   : calc
    Created on : 2 Dec 2020, 14:21:37
    Author     : simon
--%>

<%@page import="com.simonwoodworth.javamavencalcinclassdemo.Calculator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello IS3313!</h1>
        <p>This is to demonstrate changes after the first commit and push.</p>
        <p><%= new java.util.Date() %></p>
        <p><%= new Calculator().add(7, 5) %></p>
        <p><%= new Calculator().subtract(7, 5) %></p>
        <p><%= new Calculator().multiply(7, 5) %></p>
        <p><%= new Calculator().divide(7, 5) %></p>
        <p><%= new Calculator().square(7) %></p>
        <p><%= new Calculator().cube(7) %></p>
        <p><%= new Calculator().modulo(7, 5) %></p>

        <h1>118425214</h1>
        <h2>Your code demonstrating correct operation of the four new methods goes below.</h2>
        <p><%= new Calculator().addThree(10,5,2) %></p>
        <p><%= new Calculator().subtractThree(10,5,2) %></p>
        <p><%= new Calculator().multiplyThree(10,5,2) %></p>
         <p><%= new Calculator().divideThree(10,5,2) %></p>

        <h2>End of your code</h2>
    </body>
</html>
